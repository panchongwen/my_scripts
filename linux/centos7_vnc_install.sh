#/bin/bash

# 测试环境   docker pull centos:7.9.2009
# 测试环境   docker run -itd --privileged --name centos7.9_vnc -p 5901:5901 -p 6901:6901 centos:7.9.2009 /usr/sbin/init
# 测试环境   docker exec -it centos7.9_vnc bash

function install_vnc
{
	# # 设置 root 密码
	# echo "please input root passwd!"
	# passwd
	
	# 安装 vnc-server
	yum install -y "tigervnc-server"

	cp /lib/systemd/system/vncserver@.service /lib/systemd/system/vncserver@:1.service
	sed -i 's/<USER>/root/' /lib/systemd/system/vncserver@:1.service

	echo "please input vncpassword"
	vncpasswd

	# 安装并使用图形界面
	yum groupinstall -y "GNOME Desktop"
	systemctl set-default graphical.target

	# 开机启动 vnc
	systemctl enable vncserver@:1.service
	systemctl start vncserver@:1.service

	# 关闭 selinux
	if [ -f /etc/selinux/config ]; then
		sed -i 's/SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
	fi
    setenforce 0

	cd /
	yum -y install git
	# git clone https://github.com/novnc/noVNC.git
	git clone https://github.com.cnpmjs.org/novnc/noVNC.git
	cd /noVNC/utils
	# git clone https://github.com/novnc/websockify
	git clone https://github.com.cnpmjs.org/novnc/websockify
	yum -y install python3
	pip3 install numpy

	echo -e "[Unit]\n\
Description=noVnc\n\
After=syslog.target network.target\n\
\n\
[Service]\n\
ExecStart=/noVNC/utils/launch.sh --vnc localhost:5901 --listen 6901\n\
\n\
[Install]\n\
WantedBy=graphical.target" > /lib/systemd/system/novnc@:1.service

  systemctl daemon-reload

	# 开机启动 noVnc
	systemctl enable novnc@:1.service
	systemctl start novnc@:1.service

	# 关闭防火墙
	systemctl disable firewalld
	systemctl stop firewalld

	theIp=`ifconfig | grep inet | head -n1 | awk '{print $2}'`
	echo "---------- success -------------"
	echo "vnc start at ${theIp}:5901"
	echo "noVnc start at ${theIp}:6901/vnc.html"
}

install_vnc
